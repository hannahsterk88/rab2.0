# Programma naam: Python Challenge Interface v1.1
# Datum: 27-03-19
# AUTHOR:
# Ignace König
# sappie.konig@gmail.com voor vragen over de code of om te melden dat je een bug in het systeem hebt gevonden. Graag met een zo specifiek mogelijke uitleg.
# Packages: Random, zit standaard in de python library
# Functie: Het programma is een risk voor 2 spelers, waar de spelers classes zijn met functies
# In dit programma heb je de volgende classes:
#   -  information: informatie over het spel, namelijk de kaarten van verschillende spelers, informatie over landen,
#                   wie welk land bezit en hoeveel troepen je krijgt als je kaarten inwisselt, wordt hier opgeslagen.
#   -  land: hier wordt informatie opgeslagen over een land. De naam, de populatie, de inwisselwaarde in sterren die in deze versie van risk geen rol speelt, aan welke landen het land grenst,
#            en 2 variabelen die slechts belangrijk zijn voor de graphics die nog niet in deze versie zijn opgenomen.
#   -  player: hierin zijn 6 verschillende functies gedefiniëerd die allemaal een mogelijke handeling in het spel weergeven.
# Als je graphics wil moet je op de laatste regel zonder indentatie print(graphicsLog) neerzetten en die output dan in de graphics module onder de variable name log zetten, die er al staat.

oddsList = [[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
[1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
[1.0, 0.417, 0.1098, 0.0265, 0.0074, 0.0009, 0.0005, 0.0001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
[1.0, 0.7501, 0.3532, 0.1965, 0.0934, 0.0448, 0.0225, 0.01, 0.0052, 0.0028, 0.0011, 0.0006, 0.0001, 0.0, 0.0, 0.0001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
[1.0, 0.9173, 0.6583, 0.4642, 0.3076, 0.2081, 0.1318, 0.083, 0.0535, 0.0336, 0.0223, 0.0144, 0.0069, 0.0038, 0.0028, 0.0016, 0.0012, 0.0005, 0.0007, 0.0001, 0.0003, 0.0002, 0.0001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
[1.0, 0.9737, 0.7843, 0.6443, 0.4764, 0.3668, 0.2522, 0.1766, 0.1194, 0.0873, 0.0603, 0.0422, 0.0257, 0.0199, 0.0112, 0.0085, 0.0047, 0.0034, 0.0024, 0.0012, 0.0007, 0.0005, 0.0005, 0.0003, 0.0002, 0.0002, 0.0, 0.0, 0.0001, 0.0],
[1.0, 0.99, 0.8925, 0.7686, 0.636, 0.5078, 0.3882, 0.2985, 0.2216, 0.1635, 0.1185, 0.0808, 0.0568, 0.0432, 0.0319, 0.0193, 0.013, 0.0085, 0.0049, 0.0037, 0.003, 0.0019, 0.0018, 0.0013, 0.0007, 0.0, 0.0001, 0.0002, 0.0003, 0.0002],
[1.0, 0.9966, 0.9385, 0.8615, 0.75, 0.6286, 0.5246, 0.4225, 0.3271, 0.2547, 0.1961, 0.1458, 0.1092, 0.0807, 0.0555, 0.0392, 0.0283, 0.0208, 0.0141, 0.0095, 0.0079, 0.0052, 0.0031, 0.0019, 0.0013, 0.001, 0.0011, 0.0004, 0.0004, 0.0002],
[1.0, 0.9991, 0.9699, 0.913, 0.8348, 0.7405, 0.6331, 0.5342, 0.4494, 0.36, 0.2845, 0.2181, 0.1681, 0.1328, 0.0999, 0.0762, 0.0555, 0.0389, 0.0302, 0.0212, 0.0166, 0.0098, 0.0081, 0.0049, 0.0039, 0.0029, 0.0018, 0.0016, 0.0009, 0.0007],
[1.0, 0.9994, 0.9812, 0.9448, 0.893, 0.8233, 0.729, 0.6409, 0.5511, 0.4601, 0.3815, 0.3033, 0.2473, 0.1934, 0.1551, 0.1163, 0.0927, 0.0658, 0.0532, 0.0388, 0.0271, 0.022, 0.0143, 0.0109, 0.0081, 0.0062, 0.0031, 0.0033, 0.0022, 0.0017],
[1.0, 0.9998, 0.9908, 0.9671, 0.9309, 0.8718, 0.8072, 0.7297, 0.6423, 0.554, 0.4818, 0.4126, 0.3335, 0.2662, 0.2233, 0.1752, 0.1367, 0.1082, 0.0812, 0.061, 0.0472, 0.0407, 0.0262, 0.022, 0.0162, 0.0099, 0.0078, 0.005, 0.0041, 0.0027],
[1.0, 0.9999, 0.9943, 0.981, 0.9544, 0.9179, 0.8596, 0.8052, 0.7207, 0.6519, 0.5705, 0.4993, 0.4081, 0.3485, 0.2933, 0.2468, 0.1935, 0.1613, 0.1177, 0.0973, 0.0756, 0.0558, 0.043, 0.0329, 0.0267, 0.0195, 0.012, 0.0127, 0.0088, 0.0068],
[1.0, 1.0, 0.9961, 0.9881, 0.9728, 0.9459, 0.9067, 0.8505, 0.7893, 0.7169, 0.6536, 0.5785, 0.5001, 0.428, 0.3629, 0.3107, 0.2535, 0.2085, 0.1674, 0.1415, 0.1121, 0.0928, 0.0733, 0.0509, 0.043, 0.0287, 0.0247, 0.0182, 0.013, 0.0111],
[1.0, 1.0, 0.9988, 0.9937, 0.982, 0.9641, 0.9354, 0.8947, 0.8423, 0.7931, 0.7247, 0.655, 0.584, 0.5101, 0.4473, 0.3965, 0.3298, 0.2744, 0.2332, 0.194, 0.1449, 0.1318, 0.0992, 0.0765, 0.0608, 0.0528, 0.0395, 0.0329, 0.0236, 0.0187],
[1.0, 1.0, 0.999, 0.9945, 0.9901, 0.9773, 0.9587, 0.9252, 0.89, 0.8442, 0.7821, 0.7159, 0.6493, 0.5879, 0.5319, 0.4555, 0.4076, 0.3337, 0.292, 0.2482, 0.2065, 0.1666, 0.1398, 0.113, 0.0897, 0.073, 0.0595, 0.0416, 0.0327, 0.0228],
[1.0, 1.0, 0.9997, 0.9977, 0.9942, 0.9845, 0.9713, 0.952, 0.9244, 0.8812, 0.8257, 0.7876, 0.7368, 0.6573, 0.5891, 0.5381, 0.4756, 0.4149, 0.3632, 0.2997, 0.2541, 0.2065, 0.1818, 0.1511, 0.1296, 0.0981, 0.081, 0.0637, 0.0499, 0.0394],
[1.0, 1.0, 0.9996, 0.9984, 0.9957, 0.9903, 0.9797, 0.9628, 0.9429, 0.913, 0.8762, 0.8319, 0.7814, 0.7262, 0.6761, 0.6062, 0.5443, 0.4921, 0.437, 0.3745, 0.3225, 0.2648, 0.2379, 0.1925, 0.1678, 0.1364, 0.1142, 0.0872, 0.0751, 0.055],
[1.0, 1.0, 0.9996, 0.9998, 0.9972, 0.9949, 0.9873, 0.9769, 0.962, 0.9386, 0.9021, 0.8661, 0.8255, 0.7907, 0.7263, 0.674, 0.6091, 0.5588, 0.4916, 0.4367, 0.3804, 0.3372, 0.2884, 0.2486, 0.1989, 0.1855, 0.1508, 0.1219, 0.0994, 0.0811],
[1.0, 1.0, 0.9999, 0.9995, 0.9984, 0.9964, 0.9922, 0.9827, 0.9722, 0.9538, 0.9345, 0.9, 0.8718, 0.8307, 0.7813, 0.7357, 0.6843, 0.6115, 0.5711, 0.509, 0.4499, 0.3952, 0.3521, 0.3007, 0.2533, 0.2183, 0.186, 0.1553, 0.131, 0.114],
[1.0, 1.0, 0.9999, 0.9997, 0.9985, 0.9979, 0.9948, 0.9882, 0.9805, 0.9668, 0.9527, 0.9305, 0.896, 0.8633, 0.8235, 0.7818, 0.7308, 0.6798, 0.6276, 0.5608, 0.517, 0.4578, 0.4021, 0.3672, 0.318, 0.2794, 0.2341, 0.2042, 0.1618, 0.1389],
[1.0, 1.0, 1.0, 1.0, 0.9997, 0.9982, 0.9967, 0.9925, 0.9879, 0.9781, 0.9642, 0.9466, 0.924, 0.8921, 0.8683, 0.827, 0.7789, 0.7291, 0.6863, 0.6168, 0.5809, 0.5251, 0.4698, 0.4135, 0.3648, 0.3284, 0.2772, 0.2514, 0.2131, 0.1747],
[1.0, 1.0, 0.9999, 1.0, 0.9995, 0.9991, 0.9982, 0.9957, 0.9905, 0.988, 0.9749, 0.9585, 0.9421, 0.9231, 0.8902, 0.8592, 0.8197, 0.7784, 0.7337, 0.6903, 0.6383, 0.5905, 0.5299, 0.4831, 0.4306, 0.3911, 0.3461, 0.2935, 0.2592, 0.2242],
[1.0, 1.0, 1.0, 0.9999, 0.9998, 0.9996, 0.9988, 0.9986, 0.9937, 0.9895, 0.9818, 0.9693, 0.9605, 0.9394, 0.9231, 0.8894, 0.864, 0.8225, 0.7776, 0.7315, 0.6922, 0.642, 0.5903, 0.5423, 0.4895, 0.444, 0.3927, 0.3552, 0.3065, 0.2662],
[1.0, 1.0, 1.0, 0.9999, 0.9999, 0.9995, 0.9992, 0.9975, 0.9957, 0.9936, 0.9873, 0.98, 0.9748, 0.9531, 0.9367, 0.9149, 0.8862, 0.8603, 0.8195, 0.7869, 0.7437, 0.6918, 0.6475, 0.6109, 0.5376, 0.4955, 0.454, 0.4014, 0.3518, 0.3259],
[1.0, 1.0, 1.0, 1.0, 1.0, 0.9999, 0.9994, 0.999, 0.9983, 0.9941, 0.9906, 0.9872, 0.9792, 0.9637, 0.947, 0.931, 0.9076, 0.8849, 0.8537, 0.8199, 0.7841, 0.7416, 0.7015, 0.6425, 0.5993, 0.56, 0.506, 0.4664, 0.4184, 0.3635],
[1.0, 1.0, 1.0, 1.0, 1.0, 0.9999, 0.9996, 0.9991, 0.9979, 0.9962, 0.994, 0.9893, 0.9822, 0.9754, 0.9632, 0.9526, 0.9269, 0.9096, 0.8857, 0.8576, 0.8254, 0.7851, 0.7371, 0.6927, 0.6477, 0.6052, 0.5602, 0.5115, 0.4641, 0.4162],
[1.0, 1.0, 1.0, 1.0, 0.9999, 1.0, 0.9999, 0.9996, 0.999, 0.9974, 0.9956, 0.995, 0.9886, 0.9831, 0.9741, 0.9595, 0.9494, 0.9326, 0.9093, 0.8837, 0.8511, 0.8171, 0.788, 0.738, 0.7022, 0.6584, 0.6156, 0.5693, 0.5184, 0.4786],
[1.0, 1.0, 1.0, 1.0, 1.0, 0.9998, 1.0, 0.9999, 0.9992, 0.9992, 0.9966, 0.9959, 0.9916, 0.9868, 0.9836, 0.9689, 0.9619, 0.9443, 0.9252, 0.9066, 0.88, 0.8551, 0.821, 0.7815, 0.7425, 0.706, 0.6572, 0.6191, 0.5669, 0.5305],
[1.0, 1.0, 1.0, 1.0, 1.0, 0.9999, 1.0, 0.9996, 0.9995, 0.9991, 0.9979, 0.9958, 0.9943, 0.9913, 0.9886, 0.9801, 0.9731, 0.9561, 0.941, 0.9246, 0.901, 0.8804, 0.856, 0.8188, 0.7873, 0.7411, 0.7109, 0.6656, 0.6205, 0.5778],
[1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.9999, 0.9994, 0.9993, 0.9992, 0.9971, 0.9957, 0.9943, 0.9912, 0.9839, 0.9765, 0.9696, 0.9547, 0.9435, 0.9227, 0.9033, 0.8734, 0.8518, 0.8178, 0.7873, 0.734, 0.7163, 0.6676, 0.6341]]

winList = [0,0]
for xyz in range(5):
    graphicsLog = []
    from random import *
    playerAmount = 2
    
    class information:
        def __init__(self,lands):
            self.cards = [[],[]]
            self.landsOwned = [[],[]]
            self.lands = lands
            self.exchangeValue = 4
    # Deze functie bestaat zodat waar eerst bij elke player functie dit uitgeschreven moet worden het veel korter kan als element van tuple a waar list a de waarde heeft van de return value.
    # De functie heeft als enige input wie er aan de beurt is.
    def giveInformation(iteration,playerAmount):
        return(information.lands, information.landsOwned[iteration%playerAmount],information.landsOwned[(iteration+1)%playerAmount],information.cards[iteration%playerAmount],information.cards[(iteration+1)%playerAmount],information.exchangeValue)


    class land:
        def __init__(self, name, clickBoxIndex):
            self.name = str(name).lower()
            self.population = 1
            self.stars = 0
            self.owner = 0
            self.connections = []
            self.populationText = 0
            self.clickBoxIndex = clickBoxIndex


    class player1:
        def useCard(self,lands,landsOwnedMine,landsOwnedYours,myCards,yourCardAmount,exchangeValueOfCards):
            return 0
            # return 0, if you return 0 with 5 cards, your cards will be automatically exchanged
            # return any possible combination of three cards
        def placeTroops(self,lands,landsOwnedMine,landsOwnedYours,myCards,yourCardAmount,exchangeValueOfCards,troopsToDeploy):
            return (choice(landsOwnedMine),randint(1,troopsToDeploy))
            # return (destination of troops, amt)
        def attack(self,lands,landsOwnedMine,landsOwnedYours,myCards,yourCardAmount,exchangeValueOfCards):
            choiceList = []
            for i in landsOwnedMine:
                if i.population > 1:
                    attack = 0
                    for j in i.connections:
                        if j not in landsOwnedMine:
                            attack = 1
                    if attack == 1:
                        choiceList += [i]
            while len(choiceList) > 0:
                y = choice(choiceList)
                for i in y.connections:
                    if i not in landsOwnedMine and y.population > 1:
                        return(y,i,randint(1,y.population-1))
            return 0
            # return 0
            # return(src,dst,amt)
        def sendExtraTroops(self,lands,landsOwnedMine,landsOwnedYours,myCards,yourCardAmount,exchangeValueOfCards,origin,destination):
            return randint(1,origin.population-1)
            # return any non-negative integer
        def defend(self,lands,landsOwnedMine,landsOwnedYours,myCards,yourCardAmount,exchangeValueOfCards,attacker, defender,amountOfAttackers):
            return 2
            # return either 1 or 2, default is 2 if return value is neither. If only one defender it won't even be asked.
        def move(self,lands,landsOwnedMine,landsOwnedYours,myCards,yourCardAmount,exchangeValueOfCards):
            if randint(0,5) == 1:
                x = choice(landsOwnedMine)
                return (x,choice(x.connections),1)
            else:
                return 0
            # return 0
            # return(src,dst,amt)
    class player2:
        def __init__(self,defenders):
            self.defenders = defenders
        def useCard(self,lands,landsOwnedMine,landsOwnedYours,myCards,yourCardAmount,exchangeValueOfCards):
            return 0
            # return 0, if you return 0 with 5 cards, your cards will be automatically exchanged
            # return any possible combination of three cards
        def placeTroops(self,lands,landsOwnedMine,landsOwnedYours,myCards,yourCardAmount,exchangeValueOfCards,troopsToDeploy):
            return (choice(landsOwnedMine),randint(1,troopsToDeploy))
            # return (destination of troops, amt)
        def attack(self,lands,landsOwnedMine,landsOwnedYours,myCards,yourCardAmount,exchangeValueOfCards):
            choiceList = []
            for i in landsOwnedMine:
                if i.population > 1:
                    attack = 0
                    for j in i.connections:
                        if j not in landsOwnedMine:
                            attack = 1
                    if attack == 1:
                        choiceList += [i]
            while len(choiceList) > 0:
                y = choice(choiceList)
                for i in y.connections:
                    if i not in landsOwnedMine and y.population > 1:
                        return (y, i, randint(1, y.population - 1))
            return 0
            # return 0
            # return(src,dst,amt)
        def sendExtraTroops(self,lands,landsOwnedMine,landsOwnedYours,myCards,yourCardAmount,exchangeValueOfCards,origin,destination):
            return randint(1,origin.population-1)
            # return any non-negative integer
        def defend(self,lands,landsOwnedMine,landsOwnedYours,myCards,yourCardAmount,exchangeValueOfCards,attacker, defender,numberOfAttackers):
            return self.defenders
            # return 1            
            # return either 1 or 2, default is 2 if return value is neither. If only one defender it won't even be asked.
        def move(self,lands,landsOwnedMine,landsOwnedYours,myCards,yourCardAmount,exchangeValueOfCards):
            returnOrders = []
            landsDone = []
            for i in landsOwnedMine:
                if i not in landsDone:
                    landsDone += [i]
                    connectedLandList = [i]
                    newlyAddedLands = 1
                    updated = 1
                    while 1:
                        if updated == 0:
                            break
                        updated = 0
                        for k in range(newlyAddedLands):
                            if k == 0:
                                newlyAddedLands = 0
                            for l in connectedLandList[-1 - k].connections:
                                if l.owner == iteration % playerAmount:
                                    if l not in connectedLandList:
                                        updated = 1
                                        newlyAddedLands += 1
                                        landsDone += [l]
                                        connectedLandList += [l]
                    inner = []
                    outer = []
                    for j in connectedLandList:
                        addToOuter = 0
                        for k in j.connections:
                            if k.owner != iteration%playerAmount:
                                addToOuter = 1
                        if addToOuter == 1:
                            outer += [j]
                        else:
                            inner += [j]
                    if len(outer) > 0:
                        for j in inner:
                            returnOrders += [(j,choice(outer),j.population-1)]
            return returnOrders+[0]
            # return 0
            # return(src,dst,amt)


    # Hier worden alle landen gedefiniëerd met hun verschillende variabelen.

    lands = []
    # If statement om alles makkelijk in te klappen
    if 1:
        india = land('india',13);india.stars = 1;lands += [india]
        nieuwguinea = land('nieuwguinea',24);nieuwguinea.stars = 1;lands += [nieuwguinea]
        madagascar = land('madagascar',21);madagascar.stars = 1;lands += [madagascar]
        argentinie = land('argentinie',35);argentinie.stars = 1;lands += [argentinie]
        vsoost = land('vsoost',30);vsoost.stars = 1;lands += [vsoost]
        alaska = land('alaska',0);alaska.stars = 1;lands += [alaska]
        nwstaten = land('nwstaten',1);nwstaten.stars = 1;lands += [nwstaten]
        groenland = land('groenland',2);groenland.stars = 1;lands += [groenland]
        alberta = land('alberta',26);alberta.stars = 1;lands += [alberta]
        ontario = land('ontario',27);ontario.stars = 2;lands += [ontario]
        vswest = land('vswest',29);vswest.stars = 1;lands += [vswest]
        oostcanada = land('oostcanada',28);oostcanada.stars = 1;lands += [oostcanada]
        centraalamerika = land('centraalamerika',31);centraalamerika.stars = 1;lands += [centraalamerika]
        venezuela = land('venezuela',32);venezuela.stars = 1;lands += [venezuela]
        peru = land('peru',34);peru.stars = 1;lands += [peru]
        brazilie = land('brazilie',33);brazilie.stars = 1;lands += [brazilie]
        noordafrika = land('noordafrika',17);noordafrika.stars = 1;lands += [noordafrika]
        centraalafrika = land('centraalafrika',18);centraalafrika.stars = 2;lands += [centraalafrika]
        zuidafrika = land('zuidafrika',19);zuidafrika.stars = 1;lands += [zuidafrika]
        oostafrika = land('oostafrika',20);oostafrika.stars = 1;lands += [oostafrika]
        egypte = land('egypte',16);egypte.stars = 1;lands += [egypte]
        westeuropa = land('westeuropa',39);westeuropa.stars = 2;lands += [westeuropa]
        grootbrittannie = land('grootbrittannie',36);grootbrittannie.stars = 1;lands += [grootbrittannie]
        ijsland = land('ijsland',3);ijsland.stars = 1;lands += [ijsland]
        scandinavie = land('scandinavie',4);scandinavie.stars = 1;lands += [scandinavie]
        noordeuropa = land('noordeuropa',37);noordeuropa.stars = 2;lands += [noordeuropa]
        zuideuropa = land('zuideuropa',38);zuideuropa.stars = 2;lands += [zuideuropa]
        rusland = land('rusland',5);rusland.stars = 2;lands += [rusland]
        middenoosten = land('middenoosten',15);middenoosten.stars = 1;lands += [middenoosten]
        afghanistan = land('afghanistan',14);afghanistan.stars = 2;lands += [afghanistan]
        oeral = land('oeral',6);oeral.stars = 2;lands += [oeral]
        siberie = land('siberie',7);siberie.stars = 2;lands += [siberie]
        jakoetsk = land('jakoetsk',8);jakoetsk.stars = 2;lands += [jakoetsk]
        kamtsjatka = land('kamtsjatka',41);kamtsjatka.stars = 1;lands += [kamtsjatka]
        irkoetsk = land('irkoetsk',10);irkoetsk.stars = 2;lands += [irkoetsk]
        japan = land('japan',40);japan.stars = 1;lands += [japan]
        mongolie = land('mongolie',9);mongolie.stars = 2;lands += [mongolie]
        china = land('china',11);china.stars = 1;lands += [china]
        zuidoostazie = land('zuidoostazie',12);zuidoostazie.stars = 1;lands += [zuidoostazie]
        indonesie = land('indonesie',25);indonesie.stars = 1;lands += [indonesie]
        westaustralie = land('westaustralie',22);westaustralie.stars = 1;lands += [westaustralie]
        oostaustralie = land('oostaustralie',23);oostaustralie.stars = 1;lands += [oostaustralie]
    # Zelfde verhaal, maakt hier connecties aan. Dit kon niet bij de vorige stap omdat dan bijvoorbeeld een connectie met china wordt aangemaakt voordat china wordt gedefiniëerd
    if 1:
        india.connections = [middenoosten, afghanistan, china, zuidoostazie]
        nieuwguinea.connections = [indonesie, oostaustralie]
        madagascar.connections = [zuidafrika, oostafrika]
        argentinie.connections = [peru, brazilie]
        vsoost.connections = [ontario, vswest, oostcanada, centraalamerika]
        alaska.connections = [nwstaten, alberta, kamtsjatka]
        nwstaten.connections = [alberta, ontario, groenland, alaska]
        groenland.connections = [ijsland, oostcanada, ontario, nwstaten]
        alberta.connections = [ontario, vswest, alaska, nwstaten]
        ontario.connections = [oostcanada, vsoost, vswest, nwstaten, groenland, alberta]
        vswest.connections = [vsoost, centraalamerika, alberta, ontario]
        oostcanada.connections = [vsoost, groenland, ontario]
        centraalamerika.connections = [vsoost, venezuela, vswest]
        venezuela.connections = [peru, brazilie, centraalamerika]
        peru.connections = [argentinie, venezuela, brazilie]
        brazilie.connections = [peru, argentinie, noordafrika, venezuela]
        noordafrika.connections = [westeuropa, egypte, zuideuropa, oostafrika, centraalafrika, brazilie]
        centraalafrika.connections = [zuidafrika, oostafrika, noordafrika]
        zuidafrika.connections = [madagascar, oostafrika, centraalafrika]
        oostafrika.connections = [madagascar, middenoosten, egypte, noordafrika, centraalafrika, zuidafrika]
        egypte.connections = [zuideuropa, middenoosten, noordafrika, oostafrika]
        westeuropa.connections = [zuideuropa, noordeuropa, grootbrittannie, noordafrika]
        grootbrittannie.connections = [ijsland, scandinavie, noordeuropa, westeuropa]
        ijsland.connections = [scandinavie, groenland, grootbrittannie]
        scandinavie.connections = [rusland, noordeuropa, grootbrittannie, ijsland]
        noordeuropa.connections = [zuideuropa, rusland, westeuropa, grootbrittannie, scandinavie]
        zuideuropa.connections = [middenoosten, rusland, noordafrika, egypte, westeuropa, noordeuropa]
        rusland.connections = [oeral, afghanistan, middenoosten, scandinavie, noordeuropa, zuideuropa]
        middenoosten.connections = [india, afghanistan, oostafrika, egypte, zuideuropa, rusland]
        afghanistan.connections = [oeral, india, china, rusland, middenoosten]
        oeral.connections = [siberie, china, rusland, afghanistan]
        siberie.connections = [irkoetsk, jakoetsk, china, mongolie, oeral]
        jakoetsk.connections = [kamtsjatka, irkoetsk, siberie]
        kamtsjatka.connections = [irkoetsk, japan, mongolie, alaska, jakoetsk]
        irkoetsk.connections = [mongolie, siberie, jakoetsk, kamtsjatka]
        japan.connections = [mongolie, kamtsjatka]
        mongolie.connections = [china, siberie, kamtsjatka, japan]
        china.connections = [india, zuidoostazie, afghanistan, oeral, siberie, mongolie]
        zuidoostazie.connections = [indonesie, india, china]
        indonesie.connections = [nieuwguinea, westaustralie, zuidoostazie]
        westaustralie.connections = [oostaustralie, indonesie]
        oostaustralie.connections = [nieuwguinea, westaustralie]


    information = information(lands)

    # Een lijst van players zodat niet een playernaam, die variabel is, wordt aangeroepen, maar een element uit de list.
    players = [player1(),player2(2)]

    # Landen worden random verdeeld over 2 spelers.
    landChoices = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41]
    for i in range(42):
        x = choice(landChoices)
        information.landsOwned[i%playerAmount] += [lands[x]]
        lands[x].owner = i%playerAmount
        landChoices.remove(x)

    # Populatie is aan het begin 1 of 2. Elk land begint altijd een spel met hetzelfde aantal troepen erop. Bijvoorbeeld voor china 1 en voor rusland 2
    for i in lands:
        i.population = i.stars
    graphicsLog = []
    for i in lands:
        graphicsLog += [i.owner]
    if 1:
        # Welke kaarten getrokken kunnen worden
        cardList = ["infanterie","cavalerie","artillerie"]*14


        # De verschillende continenten.
        azie = [zuidoostazie,afghanistan,china,jakoetsk,irkoetsk,oeral,japan,kamtsjatka,india,mongolie,middenoosten,siberie]
        europa = [grootbrittannie,ijsland,noordeuropa,westeuropa,zuideuropa,rusland,scandinavie]
        noordamerika = [nwstaten,alaska,oostcanada,vsoost,vswest,groenland,centraalamerika,ontario,alberta]
        zuidamerika = [peru,brazilie,argentinie,venezuela]
        afrika = [centraalafrika,madagascar,noordafrika,oostafrika,zuidafrika,egypte]
        oceanie = [oostaustralie,nieuwguinea,westaustralie,indonesie]

        continents = [azie,europa,noordamerika,zuidamerika,afrika,oceanie]
        # Hoeveel troepen je er bij mag zetten als je een bepaald continent hebt.
        continentTroopValue = [7,5,5,2,3,2]
        differentCardsList = ["artillerie","cavalerie","infanterie"]

    # bepaal wie er begint (0 of ander even getal = speler 2, 1 of ander oneven getal = speler 1, xyz = om de beurt)
    iteration = xyz

    while 1:

        # Kijkt of er meer dan 1 eigenaar is, zo ja dan gebeurt er niks zo nee dan is er een winnaar.
        differentOwners = []
        for i in lands:
            if i.owner not in differentOwners:
                differentOwners += [i.owner]
        if len(differentOwners) == 1:
            winner = (iteration-1)%playerAmount
            winList[winner] += 1
            break

        if iteration == 10:

          myArmies = []
          myLands = []
          for i in information.landsOwned[0]:
            myLands += [i.name]
            myArmies += [i.population]
          # print(myLands)
          # print(myArmies)

          dangerList = []
          for i in information.landsOwned[0]:
            dangerLevel = 0
            for j in i.connections:
              if j not in information.landsOwned[0]:
                dangerLevel += j.population
                dangerList += [[i.name, dangerLevel]]
          # print(myLands)
          # print(myArmies)

        # Kent hier een troopsToDeploy waarde toe door het aantal landen door 3 te delen, te vragen of je kaarten in wil wisselen en te kijken of je continenten bezit.
        cardEarned = 0
        troopsToDeploy = 0
        x = len(information.landsOwned[iteration%playerAmount])
        troopsToDeploy += int(x/3)
        if x < 9:
            troopsToDeploy = 3
        number = 0
        for i in continents:
            tempListOfOwners = []
            for j in i:
                if j.owner not in tempListOfOwners:
                    tempListOfOwners += [j.owner]
            if len(tempListOfOwners) == 1 and tempListOfOwners[0] == iteration%playerAmount:
                troopsToDeploy += continentTroopValue[number]
            number += 1
        uniqueCards = []
        for i in information.cards[iteration%playerAmount]:
            if i not in uniqueCards:
                uniqueCards += [i]
        numberOfCards = [0 for i in range(3)]
        for i in information.cards[iteration%playerAmount]:
            numberOfCards[differentCardsList.index(i)]+= 1
        offerOption = 0
        for i in numberOfCards:
            if i > 2:
                offerOption = 1
        if len(uniqueCards) == 3:
            offerOption = 1
        if offerOption == 1:
            a = giveInformation(iteration, playerAmount)
            usedCards = players[iteration % playerAmount].useCard(a[0], a[1], a[2], a[3], a[4], a[5])
            if usedCards != 0 and (type(usedCards) == list or type(usedCards) == tuple):
                if len(usedCards) == 3:
                    differentCardsList = []
                    for i in usedCards:
                        if i not in differentCardsList:
                            differentCardsList += [i]
                    if len(differentCardsList) == 3 or len(differentCardsList) == 1:
                        troopsToDeploy += information.exchangeValue
                        information.exchangeValue += 2
                        for i in usedCards:
                            information.cards[iteration % playerAmount].remove(i)
            if len(information.cards[iteration%playerAmount]) == 5:
                if len(uniqueCards) == 3:
                    for i in differentCardsList:
                        information.cards[iteration % playerAmount].remove(i)
                else:
                    for i in range(len(numberOfCards)):
                        if numberOfCards[i] > 2:
                            for j in range(3):
                                information.cards[iteration % playerAmount].remove(differentCardsList[i])
                troopsToDeploy += information.exchangeValue
                information.exchangeValue += 2

        # Vraagt hier waar de spelers troepen neer willen zetten.
        while troopsToDeploy > 0:
            a = giveInformation(iteration,playerAmount)
            x = players[iteration % playerAmount].placeTroops(a[0],a[1],a[2],a[3],len(a[4]),a[5],troopsToDeploy)
            if len(x) == 2:
                if x[0] in information.landsOwned[iteration%playerAmount]:
                    if x[1] > 0 and type(x[1]) == int and x[1] <= troopsToDeploy:
                        x[0].population += x[1]
                        troopsToDeploy -= x[1]
                        graphicsLog += [["placeTroops",lands.index(x[0]),x[1]]]


        while 1:
            # Er gevraagd of de speler wil aanvallen.
            a = giveInformation(iteration, playerAmount)
            x = players[iteration % playerAmount].attack(a[0], a[1], a[2], a[3], len(a[4]),a[5])
            # Hij kijkt of de return value mogelijk is en vraagt dan aan de andere speler met hoeveel man te verdedigen.
            y=-1
            if x != 0 and len(x) == 3:
                if x[0] in information.landsOwned[iteration%playerAmount] and x[1] in information.landsOwned[(iteration+1)%playerAmount]:
                    numberOfAttackers = x[2]
                    if type(numberOfAttackers) == int and numberOfAttackers > 3:
                        numberOfAttackers = 3
                    if numberOfAttackers >= x[0].population:
                        continue
                minimumSend = numberOfAttackers
                if x[1].population > 1:
                    # Hier wordt gevraagd hoeveel verdedigers speler 2 wil gebruiken. Dan wordt gekeken of de output mogelijk is en wordt de aanval uitgewerkt.
                    a = giveInformation(iteration, playerAmount)
                    numberOfDefenders = players[iteration % playerAmount].defend(a[0], a[1], a[2], a[4], len(a[3]),a[5],x[0],x[1],numberOfAttackers)
                    if numberOfDefenders != 1 and numberOfDefenders != 2:
                        numberOfDefenders = 2
                else:
                    numberOfDefenders = 1
                attackerRolls =  []
                for k in range(numberOfAttackers):
                    attackerRolls += [randint(1,6)]
                defenderRolls = []
                for k in range(numberOfDefenders):
                    defenderRolls += [randint(1,6)]
                while len(attackerRolls) > 0 and len(defenderRolls) > 0:
                    v = max(attackerRolls)
                    w = max(defenderRolls)
                    if v > w:
                        x[1].population -= 1
                    else:
                        x[0].population -= 1;minimumSend -= 1
                    attackerRolls.remove(v)
                    defenderRolls.remove(w)
                capturedLand = 0
                if x[1].population == 0:
                    capturedLand = 1
                    if cardEarned == 0 and len(cardList)>0:
                        z = randint(0,len(cardList)-1)
                        information.cards[iteration%playerAmount] += [cardList[z]]
                        del cardList[z]
                    cardEarned = 1
                    x[1].owner = iteration%playerAmount
                    information.landsOwned[(iteration+1)%playerAmount].remove(x[1])
                    information.landsOwned[iteration % playerAmount] += [x[1]]
                    x[1].population = minimumSend
                    x[0].population -= minimumSend
                    if x[0].population > 1:
                        a = giveInformation(iteration, playerAmount)
                        y = players[iteration % playerAmount].sendExtraTroops(a[0], a[1], a[2], a[3], len(a[4]),a[5],x[0],x[1])
                        if x[0].population > y and type(y) == int and y >= 0:
                            x[0].population -= y
                            x[1].population += y
                graphicsLog += [["attack",lands.index(x[0]),lands.index(x[1]),x[0].population,x[1].population,capturedLand]]
            else:
                break

        while 1:
            # Hier wordt gevraagd of de speler wil bewegen met zijn troepen.
            a = giveInformation(iteration, playerAmount)
            x = players[iteration % playerAmount].move(a[0], a[1], a[2], a[3], len(a[4]),a[5])
            if not (type(x) == list or type(x) == tuple):
                x = [x]
            elif x[0] in lands:
                x = [x]
            for i in x:
                if i != 0:
                    connectedLandList = [i[0]]
                    newlyAddedLands = 1
                    updated = 1
                    while 1:
                        if updated == 0:
                            break
                        updated = 0
                        for k in range(newlyAddedLands):
                            if k == 0:
                                newlyAddedLands = 0
                            for l in connectedLandList[-1-k].connections:
                                if l.owner == iteration%playerAmount:
                                    if l not in connectedLandList:
                                        updated = 1
                                        newlyAddedLands += 1
                                        connectedLandList += [l]
                        if i[1] in connectedLandList and i[2]<i[0].population:
                            i[1].population += i[2]
                            i[0].population -= i[2]
                            graphicsLog += [["move",lands.index(i[0]),lands.index(i[1]),i[2]]]
                            break
                else:
                    break
            if x[-1]== 0:
                break
        iteration += 1

print(winList)
# print(graphicsLog)